<?php

namespace Tests;

use App\engageme\Users\Models\Users;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    /**
     * @var Users
     */
    protected $user;

    /**
     * Set up the test.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(Users::class)->create();
    }

    /**
     * Reset the migrations.
     */
    public function tearDown(): void
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }
}
