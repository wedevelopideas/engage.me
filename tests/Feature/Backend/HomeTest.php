<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class HomeTest extends TestCase
{
    /** @test */
    public function test_shows_backend_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('backend'));
        $response->assertSuccessful();
    }
}
