<?php

namespace Tests\Feature;

use Tests\TestCase;

class SettingsTest extends TestCase
{
    /** @test */
    public function test_shows_settings_dashboard()
    {
        $response = $this->actingAs($this->user)->get(route('settings'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_account_settings()
    {
        $response = $this->actingAs($this->user)->get(route('settings.account'));
        $response->assertSuccessful();
    }
}
