@extends('layouts.frontend')
@section('title', 'engage.me')
@section('content')
    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-2">
                    <div class="tag">
                        {{ auth()->user()->name }}
                    </div>
                    <figure class="image">
                        <img src="{{ auth()->user()->profilepic('original') }}" alt="{{ auth()->user()->name }}">
                    </figure>
                    <div class="buttons has-addons">
                        @if (auth()->user()->type === 'admin')
                            <a href="{{ route('backend') }}" class="button is-fullwidth is-radiusless">
                                <span class="icon">
                                    <i class="fas fa-tachometer-alt"></i>
                                </span>
                                <span>
                                    {{ trans('common.backend') }}
                                </span>
                            </a>
                        @endif
                        <a href="{{ route('settings') }}" class="button is-fullwidth is-radiusless">
                            <span class="icon">
                                <i class="fas fa-cogs"></i>
                            </span>
                            <span>
                                {{ trans('common.settings') }}
                            </span>
                        </a>
                    </div>
                </div>
                <div class="column is-10">
                </div>
            </div>
        </div>
    </section>
@endsection