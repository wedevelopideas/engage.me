<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/app.css') }}">
    <script src="{{ asset('assets/jquery.min.js') }}"></script>
    @yield('styles')
    <script>
        $(document).ready(function () {

            // Check for click events on the navbar burger icon
            $(".navbar-burger").click(function () {

                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                $(".navbar-burger").toggleClass("is-active");
                $(".navbar-menu").toggleClass("is-active");

            });
        });
    </script>
    @yield('scripts')
</head>
<body>
<nav class="navbar is-black">
    <div class="navbar-brand">
        <a href="{{ route('backend') }}" class="navbar-item">
            engage.me
        </a>
        <a role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>
    <div class="navbar-menu" id="navMenu">
        <div class="navbar-end">
            <a href="{{ route('home') }}" class="navbar-item">
                <span class="icon">
                    <i class="fas fa-tachometer-alt"></i>
                </span>
            </a>
        </div>
    </div>
</nav>
@yield('content')
</body>
</html>