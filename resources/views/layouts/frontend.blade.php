<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/app.css') }}">
    <script src="{{ asset('assets/jquery.min.js') }}"></script>
    @yield('styles')
    <script>
        $(document).ready(function() {

            // Check for click events on the navbar burger icon
            $(".navbar-burger").click(function() {

                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                $(".navbar-burger").toggleClass("is-active");
                $(".navbar-menu").toggleClass("is-active");

            });
        });
    </script>
    @yield('scripts')
</head>
<body>
<nav class="navbar is-black">
    <div class="container">
        <div class="navbar-brand">
            <a href="{{ route('home') }}" class="navbar-item">
                engage.me
            </a>
            <a role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>
        <div class="navbar-menu" id="navMenu">
            <div class="navbar-end">
                <div class="navbar-item has-dropdown is-hoverable">
                    <div class="navbar-link">
                        {{ auth()->user()->first_name }}
                    </div>
                    <div class="navbar-dropdown">
                        <a href="{{ route('settings') }}" class="navbar-item">
                            <span class="icon">
                                <i class="fas fa-cogs"></i>
                            </span>
                            <span>{{ trans('common.settings') }}</span>
                        </a>
                        <a href="{{ route('logout') }}" class="navbar-item">
                            <span class="icon">
                                <i class="fas fa-sign-out-alt"></i>
                            </span>
                            <span>{{ trans('common.logout') }}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
@yield('content')
<footer class="footer">
    <div class="container">
        <div class="columns">
            <div class="column is-3 is-offset-2">
                <h4><strong>Group 1</strong></h4>
                <ul>
                    <li><a href="#" class="item">Link One</a></li>
                    <li><a href="#" class="item">Link Two</a></li>
                    <li><a href="#" class="item">Link Three</a></li>
                    <li><a href="#" class="item">Link Four</a></li>
                </ul>
            </div>
            <div class="column is-3">
                <h4><strong>Group 2</strong></h4>
                <ul>
                    <li><a href="#" class="item">Link One</a></li>
                    <li><a href="#" class="item">Link Two</a></li>
                    <li><a href="#" class="item">Link Three</a></li>
                    <li><a href="#" class="item">Link Four</a></li>
                </ul>
            </div>
            <div class="column is-4">
                <h4><strong>Group 3</strong></h4>
                <ul>
                    <li><a href="#" class="item">Link One</a></li>
                    <li><a href="#" class="item">Link Two</a></li>
                    <li><a href="#" class="item">Link Three</a></li>
                    <li><a href="#" class="item">Link Four</a></li>
                </ul>
            </div>
        </div>
        <div class="content has-text-centered">
            <h4>Footer Header</h4>
            <p>Extra space for a call to action inside the footer that could help re-engage users.</p>
        </div>
        <div class="level">
            <div class="level-item">
                <a class="item" href="#">Site Map</a> /
                <a class="item" href="#">Contact Us</a> /
                <a class="item" href="#">Terms and Conditions</a> /
                <a class="item" href="#">Privacy Policy</a>
            </div>
        </div>
    </div>
</footer>
</body>
</html>