@extends('layouts.blank')
@section('title', 'engage.me')
@section('content')
    <section class="hero is-fullheight">
        <div class="hero-head">
            <div class="navbar">
                <div class="container">
                    <div class="navbar-brand">
                        <a href="{{ route('index') }}" class="navbar-item">
                            engage.me
                        </a>
                        <span class="navbar-burger burger" data-target="navbarMenu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </div>
                    <div id="navbarMenu" class="navbar-menu">
                        <div class="navbar-end">
                            <span class="navbar-item">
                                <a href="{{ route('login') }}" class="button is-outlined">
                                    <span class="icon">
                                        <i class="fas fa-sign-in-alt"></i>
                                    </span>
                                    <span>{{ trans('common.login') }}</span>
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-6 is-offset-3">
                    <h1 class="title">
                        Coming Soon
                    </h1>
                    <h2 class="subtitle">
                        $this is the best software platform for running an internet business. We handle billions of dollars every year for forward-thinking businesses around the world.
                    </h2>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {

            // Check for click events on the navbar burger icon
            $(".navbar-burger").click(function() {

                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                $(".navbar-burger").toggleClass("is-active");
                $(".navbar-menu").toggleClass("is-active");

            });
        });
    </script>
@endsection