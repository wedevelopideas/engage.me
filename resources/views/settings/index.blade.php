@extends('layouts.frontend')
@section('title', 'Settings - engage.me')
@section('content')
    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-full">
                    <h1 class="title is-1">
                        {{ trans('common.settings') }}
                    </h1>
                </div>
            </div>
            <div class="columns">
                <div class="column is-2">
                    <div class="tag">
                        {{ auth()->user()->name }}
                    </div>
                    <figure class="image">
                        <img src="{{ auth()->user()->profilepic('original') }}" alt="{{ auth()->user()->name }}">
                    </figure>
                </div>
                <div class="column is-10">
                    <div class="list">
                        <div class="list-item">
                            <div>
                                <a href="{{ route('settings.account') }}">
                                    <span class="icon">
                                        <i class="fas fa-user"></i>
                                    </span>
                                    <span>{{ trans('settings.account') }}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection