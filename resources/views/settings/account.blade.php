@extends('layouts.frontend')
@section('title', 'Settings - engage.me')
@section('content')
    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-full">
                    <h1 class="title is-1">
                        {{ trans('common.settings') }}
                    </h1>
                </div>
            </div>
            <div class="columns">
                <div class="column is-2">
                    <div class="tag">
                        {{ auth()->user()->name }}
                    </div>
                    <figure class="image">
                        <img src="{{ auth()->user()->profilepic('original') }}" alt="{{ auth()->user()->name }}">
                    </figure>
                </div>
                <div class="column is-10">
                    <form action="{{ route('settings.account') }}" method="post">
                        @csrf

                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label" for="gender">{{ trans('common.gender') }}</label>
                            </div>
                            <div class="field-body">
                                <div class="field is-narrow">
                                    <div class="control">
                                        <label class="radio">
                                            <input type="radio" name="gender"
                                                   {{ ($user->gender === 'female') ? ' checked' : '' }} tabindex="0">
                                            {{ trans('common.female') }}
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="gender"
                                                   {{ ($user->gender === 'male') ? ' checked' : '' }} tabindex="0">
                                            {{ trans('common.male') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label">{{ trans('common.name') }}</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input type="text" class="input" name="first_name" id="first_name"
                                               placeholder="{{ trans('common.first_name') }}"
                                               value="{{ $user->first_name }}">
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="control">
                                        <input type="text" class="input" name="last_name" id="last_name"
                                               placeholder="{{ trans('common.last_name') }}"
                                               value="{{ $user->last_name }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label" for="birthday">{{ trans('common.birthday') }}</label>
                            </div>
                            <div class="field-body">
                                <div class="control">
                                    <input type="date" class="input" name="birthday" id="birthday"
                                           placeholder="{{ trans('common.birthday') }}"
                                           value="{{ $user->birthday->format('Y-m-d') }}">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection