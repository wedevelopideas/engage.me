@extends('layouts.blank')
@section('title', 'Login | engage.me')
@section('content')
    <section class="hero is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-4 is-offset-4">
                    <h3 class="title has-text-grey">{{ trans('common.login') }}</h3>
                    <p class="subtitle has-text-grey">Log-in to your account</p>
                    <div class="box">
                        <form action="{{ route('login') }}" method="post">
                            @csrf

                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input" name="email" placeholder="{{ trans('common.email') }}" autocomplete="email" autofocus>
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input type="password" class="input" name="password" placeholder="{{ trans('common.password') }}" autocomplete="current-password">
                                </div>
                            </div>

                            <button type="submit" class="button is-block is-info is-large is-fullwidth">{{ trans('common.login') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection