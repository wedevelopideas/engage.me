@if ($errors->any())
<div class="ui icon error message">
    <i class="x icon"></i>
    <div class="content">
        <ul class="list">
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
</div>
@endif

@if (session()->has('error'))
    <div class="ui icon error message">
        <i class="x icon"></i>
        <div class="content">
            <p>{{ session()->get('error') }}</p>
        </div>
    </div>
@endif