<?php

return [
    'backend' => 'Backend',
    'birthday' => 'Birthday',
    'email' => 'Email',
    'female' => 'Female',
    'first_name' => 'First name',
    'gender' => 'Gender',
    'last_name' => 'Last name',
    'login' => 'Sign in',
    'logout' => 'Sign out',
    'male' => 'Male',
    'name' => 'Name',
    'password' => 'Password',
    'settings' => 'Settings',
];
