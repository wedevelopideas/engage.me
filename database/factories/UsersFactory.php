<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\engageme\Users\Models\Users;

$factory->define(Users::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('password'),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'name' => $faker->name,
        'user_name' => $faker->unique()->name,
        'gender' => $faker->randomElement(['male', 'female']),
        'birthday' => $faker->date('Y-m-d'),
        'type' => $faker->text,
        'remember_token' => Str::random(10),
    ];
});
