<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use App\engageme\Users\Models\Users;
use App\engageme\Users\Models\UsersAvatars;

$factory->define(UsersAvatars::class, function (Faker $faker) {
    return [
        'user_id' => factory(Users::class)->create()->id,
        'size' => $faker->name,
        'url' => $faker->imageUrl(),
    ];
});
