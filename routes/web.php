<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Backend', 'prefix' => 'backend', 'middleware' => 'auth'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'backend']);
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('', ['uses' => 'IndexController@index', 'as' => 'index']);

    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', ['uses' => 'LoginController@showLogin', 'as' => 'login']);
        Route::post('login', ['uses' => 'LoginController@handleLogin']);
        Route::get('logout', ['uses' => 'LoginController@handleLogout', 'as' => 'logout']);
    });

    Route::get('home', ['uses' => 'HomeController@home', 'as' => 'home']);

    Route::group(['prefix' => 'settings'], function () {
        Route::get('', ['uses' => 'SettingsController@index', 'as' => 'settings']);
        Route::get('account', ['uses' => 'SettingsController@account', 'as' => 'settings.account']);
    });
});
