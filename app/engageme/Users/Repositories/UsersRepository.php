<?php

namespace App\engageme\Users\Repositories;

use App\engageme\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param  Users  $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * Get the user by its id.
     *
     * @param  int  $id
     * @return mixed
     */
    public function getUserByID(int $id)
    {
        return $this->users
            ->where('id', $id)
            ->first();
    }
}
