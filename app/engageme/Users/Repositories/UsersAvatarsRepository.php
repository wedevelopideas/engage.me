<?php

namespace App\engageme\Users\Repositories;

use App\engageme\Users\Models\UsersAvatars;

class UsersAvatarsRepository
{
    /**
     * @var UsersAvatars
     */
    private $usersAvatars;

    /**
     * UsersAvatarsRepository constructor.
     * @param  UsersAvatars  $usersAvatars
     */
    public function __construct(UsersAvatars $usersAvatars)
    {
        $this->usersAvatars = $usersAvatars;
    }
}
