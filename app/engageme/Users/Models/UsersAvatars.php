<?php

namespace App\engageme\Users\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\engageme\Users\Models\UsersAvatars.
 *
 * @property int $id
 * @property int $user_id
 * @property string $size
 * @property string $url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class UsersAvatars extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'size',
        'url',
    ];

    /**
     * Returns the relationship with users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(Users::class, 'user_id', 'id');
    }
}
