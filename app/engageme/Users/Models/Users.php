<?php

namespace App\engageme\Users\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\engageme\Users\Models\Users.
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $name
 * @property string $user_name
 * @property string $gender
 * @property \Illuminate\Support\Carbon|null $birthday
 * @property string $type
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 */
class Users extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'name',
        'user_name',
        'gender',
        'birthday',
        'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'birthday',
    ];

    /**
     * Returns the relationship with avatars.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function avatars()
    {
        return $this->hasOne(UsersAvatars::class, 'user_id', 'id');
    }

    /**
     * Returns the required size of the user's avatar.
     *
     * @param  string  $size
     * @return mixed
     */
    public function profilepic(string $size)
    {
        $query = $this->avatars()
            ->where('size', $size)
            ->first();

        if ($query) {
            return $query->url;
        }
    }
}
