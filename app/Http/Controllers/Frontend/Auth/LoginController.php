<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\engageme\Users\Requests\LoginRequest;

class LoginController extends Controller
{
    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('handleLogout');
    }

    /**
     * Shows the login form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLogin()
    {
        return view('auth.login');
    }

    /**
     * Handles the login request.
     *
     * @param  LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleLogin(LoginRequest $request)
    {
        $details = $request->only('email', 'password');

        if (auth()->attempt($details)) {
            return redirect()
                ->route('home');
        }

        return redirect()
            ->route('login')
            ->with('error', trans('errors.login_failed'));
    }

    /**
     * Handles the logout request.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleLogout()
    {
        auth()->logout();

        return redirect()
            ->route('index');
    }
}
