<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\engageme\Users\Repositories\UsersRepository;

class SettingsController extends Controller
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * SettingsController constructor.
     * @param  UsersRepository  $usersRepository
     */
    public function __construct(UsersRepository $usersRepository)
    {
        $this->middleware('auth');
        $this->usersRepository = $usersRepository;
    }

    /**
     * Shows the dashboard of the user settings.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $id = auth()->id();

        $user = $this->usersRepository->getUserByID($id);

        return view('settings.index')
            ->with('user', $user);
    }

    /**
     * Shows the account settings of the user.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function account()
    {
        $id = auth()->id();

        $user = $this->usersRepository->getUserByID($id);

        return view('settings.account')
            ->with('user', $user);
    }
}
